# Docker Workshop - Windows
Lab 06: Docker Compose

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir lab-06
$ cd lab-06
```

## Instructions

- Create a "docker-compose.yml" file with the content below:

```
version: '2.4'
services:
  traefik:
    image: stefanscherer/traefik-windows
    command: --docker.endpoint=npipe:////./pipe/docker_engine --logLevel=DEBUG
    ports:
      - "8080:8080"
      - "443:443"
      - "80:80"
    isolation: process # workaround for https://github.com/containous/traefik/issues/4409
    volumes:
      - .:C:/etc/traefik
      - type: npipe
        source: \\.\pipe\docker_engine
        target: \\.\pipe\docker_engine

  whoami1:
    image: stefanscherer/whoami
    labels:
      - "traefik.backend=whoami"
      - "traefik.frontend.entryPoints=http"
      - "traefik.frontend.rule=Host:whoami.docker.local"

  whoami2:
    image: stefanscherer/whoami
    labels:
      - "traefik.backend=whoami"
      - "traefik.frontend.entryPoints=http"
      - "traefik.frontend.rule=Host:whoami.docker.local"

  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    labels:
      - "traefik.backend=portainer"
      - "traefik.frontend.rule=Host:portainer.yourdomain.com"

networks:
  default:
    external:
      name: nat
```
 
 - Start all the environment in detached mode:

```
 $ docker-compose up -d
```

 - Check the running containers:

```
$ docker-compose ps
```

 - Wait until docker complete to start the containers, meanwhile let's inspect the wordpress logs:

```
$ docker-compose logs -f traefik
```

 - Browse to the portainer application:
 
```
http://server-ip:9000
```
  
 - Enter to the "whoami1" container:
   
```
$ docker-compose exec whoami1 cmd
```

 - Inspect the container filesystem:
   
```
$ dir
```

 - Exit from the container
```
$ exit
```

 - Scale the service "whoami2" to 5 container:

```
$ docker-compose scale whoami2=5
```

 - Check the running containers:

```
$ docker-compose ps
```

 - Let's clean our environment (remove the containers defined in the docker-compose file):

```
$ docker-compose down
```

 - Check the running containers:

```
$ docker-compose ps
```
 